from django.contrib import admin

# Register your models here.

from django.contrib import admin
# Im Tutorial werden hier beide Klassen direkt importiert.
# Ich finde es so übersichtlicher und muss nicht jede Klasse
# an zwei Stellen eintragen.
from . import models
#from .models import Choice

admin.site.register(models.Question) # Hier registrieren wir unser Model
admin.site.register(models.Choice)

class ChoiceInline(admin.TabularInline):
    model = models.Choice
    extra = 0

class QuestionAdmin(admin.ModelAdmin):
    inlines = [ ChoiceInline ]
