from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def detail(request, question_id):
  # do something with the question_id
  question = get_object_or_404(Question, pk=question_id)
  # Provide some context
  context = {
    "question": question
  }
  return render(request, "polls/results.html", context)
